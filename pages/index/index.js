//index.js
//获取应用实例

var app = getApp()
Page({
  data: {
    home: 'top-hoverd-btn',
    new: '',
    play: '',
    imgUrls: [
      'http://ifanr-cdn.b0.upaiyun.com/wp-content/uploads/2016/10/Mavic-fly.gif!1120',
      'http://ifanr-cdn.b0.upaiyun.com/wp-content/uploads/2016/09/mbp1.jpg!1120',
      'http://ifanr-cdn.b0.upaiyun.com/wp-content/uploads/2016/09/mars18a.jpg!1120',
      'http://ifanr-cdn.b0.upaiyun.com/wp-content/uploads/2016/10/DNS1.jpg!1120',
      'http://ifanr-cdn.b0.upaiyun.com/wp-content/uploads/2016/10/mk.png!1120',
      'http://ifanr-cdn.b0.upaiyun.com/wp-content/uploads/2016/10/6.pic_.jpg!1120'
    ],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    content: [{
      pubDate: "2016-10-04 12:31:34",
      title: "Ta们说：索尼不会放弃手机业务网友咋看",
      url: "http://img5.pcpop.com/ArticleImages/730x547/4/4077/004077906.jpg",
    }, {
        pubDate: "2016-10-05 22:20:31",
        title: "黑客公布对物联网僵尸网络恶意软件Mirai源代码",
        url: "http://cimage.tianjimedia.com/uploadImages/2016/10/20161004211322535001.png"
      }, {
        pubDate: "2016-10-05 13:32:05",
        title: "苹果旗下Beats公司CMO将于下月离职",
        url: "http://uppic.fd.zol-img.com.cn/t_s500x2000/g5/M00/0A/06/ChMkJ1f00D2ISWVwAABo9iDt7r4AAWmqwMkei0AAGkO172.jpg"
      }]
  },
  //事件处理函数
  updateBtnStatus: function (k) {
    this.setData({
      new: this.getHoverd('new', k),
      home: this.getHoverd('home', k),
      play: this.getHoverd('play', k),
    });
  },
  getHoverd: function (src, dest) {
    return (src === dest ? 'top-hoverd-btn' : '');
  },
  toNew: function () {
    this.updateBtnStatus('new');
  },
  toHome: function () {
    this.updateBtnStatus('home');
  },
  toPlay: function () {
    this.updateBtnStatus('play');
  },
  viewDetail: function () {
    wx.navigateTo({
      url: '../detail/detail'
    })
  },
  onLoad: function () {
    console.log('onLoad')
    var that = this
    //调用应用实例的方法获取全局数据
    app.getUserInfo(function (userInfo) {
      //更新数据
      that.setData({
        userInfo: userInfo
      })
      that.update()
    })
  }
})
